#-----------------------------------------------------------------------
#------------------------------Item Logger------------------------------
#-----------------------------------------------------------------------
#
#---------------------------History Retriever---------------------------

$logLocation = "%userprofile%\AppData\LocalLow\miHoYo\Genshin Impact\output_log.txt";
$path = [System.Environment]::ExpandEnvironmentVariables($logLocation);
if (-Not [System.IO.File]::Exists($path)) {
    Write-Host "Cannot find the log file!" -ForegroundColor Red
    return
}

$logs = Get-Content -Path $path
$match = $logs -match "^OnGetWebViewPageFinish.*log$"
if (-Not $match) {
    Write-Host "Cannot find the wish history url!" -ForegroundColor Red
    return
}

[string] $wishHistoryUrl = $match -replace 'OnGetWebViewPageFinish:', ''
Write-Host $wishHistoryUrl
Write-Host "Link obtained" -ForegroundColor Green
#Set-Clipboard -Value $wishHistoryUrl
#Write-Host "Link copied to clipboard" -ForegroundColor Green

#---------------------------Check If Already Updated---------------------------

$today = Get-Date -Format "dddd MM/dd/yyyy"
Write-Host "Today's Date:" $today
$dateCheck = Get-Content C:\Users\Forgy\Documents\WishesList.txt | %{$_ -match $today}
if ($dateCheck -contains "*True*"){
    Write-Host "You Already Updated Today" -ForegroundColor Red
}
else{

    #---------------------------Add New Date---------------------------

    Add-Content -Path C:\Users\Forgy\Documents\WishesList.txt "-----------------------------------------------"
    Add-Content -Path C:\Users\Forgy\Documents\WishesList.txt -Value($today)
    Add-Content -Path C:\Users\Forgy\Documents\WishesList.txt "-----------------------------------------------"

    #---------------------------Store Link---------------------------

    Add-Content -Path C:\Users\Forgy\Documents\WishesList.txt $wishHistoryUrl
    Write-Host "WishesListUpdated" -ForegroundColor Green
}

