
terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.8.0"
    }
  }
}

variable "gitlab_access_token" {
    type = string
}

provider "gitlab" {
    token = var.gitlab_access_token
}

data "gitlab_project" "example_project" {
    id = 33363760
}

resource "gitlab_project_variable" "sample_project_variable" {
    project = data.gitlab_project.example_project.id
    key = "TF_VAR_gitlab_access_token"
    value = "Forgy"
}